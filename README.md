# Fetch Rewards Coding Exercise

## Description
This C# program is a solution for solving a fake coin puzzle using Selenium WebDriver. The goal of the puzzle is to find a fake gold bar among real ones of uniform size and weight using a balance scale. I approached solving this puzzle using a binary search method.
## Requirements
- .NET Core SDK
- ChromeDriver
- Selenium WebDriver for .NET

## Setup
1. Clone this repository.
2. Install .NET Core SDK if not already installed.
3. Download ChromeDriver and place it in the project directory.
4. Install Selenium WebDriver for .NET via NuGet Package Manager.

## Usage
1. Run the program.
2. The program will automatically navigate to the puzzle page, reset the puzzle, and start solving it. 
3. Wait for the program to complete the solving process.

## Details
- The program utilizes Selenium WebDriver with ChromeDriver to automate interactions with the puzzle webpage.
- It implements a binary search algorithm to efficiently find the fake coin.
- The program clicks on the coins based on the comparison results obtained from the weighing.

## Code Structure
- `SendKeysToBoard`: Function to populate the game boards given a list of coin values.
- `binarySearch`: Function to perform binary search to find the fake coin.
- Main: Initialization, setup, and execution of the binary search algorithm.

## Notes
- Ensure that the Selenium WebDriver dependencies are properly configured and ChromeDriver is compatible with your Chrome browser version.

## Credits
This program was created by Henry Leaders.
