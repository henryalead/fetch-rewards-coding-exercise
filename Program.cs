using OpenQA.Selenium.Chrome;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System.Xml.Linq;

//Initializing variables and starting the Selenium Driver
IWebDriver driver = new ChromeDriver();
driver.Url = "http://sdetchallenge.fetch.com/";
driver.Navigate();
IWebElement resetButton = driver.FindElement(By.CssSelector("#reset:not([disabled])"));
IWebElement weighButton = driver.FindElement(By.Id("weigh"));
var coinElements = driver.FindElements(By.CssSelector("[id*=coin]"));


void SendKeysToBoard(IWebDriver driver, string boardSide, List<int> keysToSend) //Function to simplify populating the game boards with a list of values
{
    var boardElements = driver.FindElements(By.CssSelector($"[id*={boardSide}].square"));
    for (int i = 0; i < keysToSend.Count; i++)
    {
        boardElements[i].SendKeys(keysToSend[i].ToString());
    }
}

int binarySearch(List<int> coinValues)
{
    resetButton.Click();
    SendKeysToBoard(driver,"left", coinValues.Take(coinValues.Count/ 2).ToList()); //Sending the first half of the given list to the first game board
    
    //Sending the second half of the list to the second board
    if(coinValues.Count%2 == 0)
    {
        SendKeysToBoard(driver, "right", coinValues.Skip(coinValues.Count / 2).ToList());
    }
    else
    {
        SendKeysToBoard(driver, "right", coinValues.Skip(coinValues.Count / 2 + 1).ToList()); //If the amount of values we are performing the binary search on is odd, we exclude the middle value from the weighing 
    }
    
    IWebElement weighingsElement = driver.FindElement(By.CssSelector("#root > div > div.game > div.game-info > ol"));
    int weighingCount = weighingsElement.FindElements(By.TagName("li")).Count;
    weighButton.Click();

    WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
    wait.Until(driver =>
    {
        return weighingsElement.FindElements(By.TagName("li")).Count > weighingCount; //Wait until the new weighing shows up on the screen
    });

    var weighings = weighingsElement.FindElements(By.TagName("li"));
    string mostRecentWeighing = weighings.Last().Text;
    if (mostRecentWeighing.Contains("="))
    {
        //If coins are the same weight, then the one we left out is the fake
        coinElements[coinValues[coinValues.Count / 2]].Click();
        return coinValues[coinValues.Count / 2];
    }
    else
    {
        if (mostRecentWeighing.Contains(">"))
        {
           //If the right side is lighter, one of the coins on the right is the fake 
           return binarySearch(coinValues.Skip(coinValues.Count / 2).ToList()); //Perform binary search on the right side's coins
        }
        else if (mostRecentWeighing.Contains("<"))
        {
            if (coinValues.Count() <= 3)
            {
                //If there are three coins in the search, only two are weighed, so lighter one is the fake 
                coinElements[coinValues[0]].Click();
                return coinValues[0];
            }
            else
            {
                //If the left side is lighter, one of the coins on the left is the fake 
                return binarySearch(coinValues.Take(coinValues.Count / 2 + 1).ToList()); //Perform binary search on the left side's coins
            }
        }
        else
        {
            throw new Exception("Could not find an operator in most recent weighing");
        }
    }
}

List<int> coinVals = coinElements.Select(e => int.Parse(e.Text)).ToList();

binarySearch(coinVals);
